use crate::config::SiteConfig;
use crate::renderer::posts::PostMetadata;
use crate::renderer::{MARKDOWN_EXT, TOML_SEP};
use crate::util::{self, Error};
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::rc::Rc;

static PATH_PAGES: &str = "_pages";
static PATH_POSTS: &str = "_posts";
static PATH_TEMPLATES: &str = "_templates";
static SLUG_TOKEN: &str = "{{slug}}";

#[derive(Debug)]
pub struct SiteStructure {
    pub pages: bool,
    pub posts: bool,
    pub templates: bool,
    pub custom: Vec<String>,
    pub posts_rec: Vec<PostRecord>,
}

impl SiteStructure {
    pub fn new() -> Self {
        SiteStructure {
            pages: false,
            posts: false,
            templates: false,
            custom: vec![],
            posts_rec: vec![],
        }
    }
}

#[derive(Debug)]
pub struct PostRecord {
    pub source: PathBuf,
    pub dest_url: String,
    pub metadata: Rc<PostMetadata>,
}

pub fn make_custom_path(base_path: &Path, custom: &str) -> PathBuf {
    base_path.join(custom).to_owned()
}

pub fn make_pages_path(base_path: &Path) -> PathBuf {
    base_path.join(PATH_PAGES).to_owned()
}

pub fn make_posts_path(base_path: &Path) -> PathBuf {
    base_path.join(PATH_POSTS).to_owned()
}

pub fn make_templates_path(base_path: &Path) -> PathBuf {
    base_path.join(PATH_TEMPLATES).to_owned()
}

pub fn make_output_path(source_tree: &Path, source: &Path, dest_tree: &Path) -> PathBuf {
    let source_tree_str = source_tree.to_str().unwrap();
    let source_str = source.to_str().unwrap();
    let source_str = String::from(&source_str[source_tree_str.chars().count() + 1..]);
    dest_tree.join(&source_str).to_owned()
}

pub fn make_output_path_with_ext(
    source_tree: &Path,
    source: &Path,
    dest_tree: &Path,
    ext: &str,
) -> PathBuf {
    let dest_path = make_output_path(source_tree, source, dest_tree);
    dest_path.with_extension(ext)
}

pub fn discover_site_structure(config: &SiteConfig) -> Result<SiteStructure, Error> {
    let mut structure = SiteStructure::new();
    let base_path = config.base_path();
    for entry in base_path
        .read_dir()
        .map_err(|e| Error(format!("Unable to read directory: {:?}", e)))?
    {
        if let Ok(entry) = entry {
            let path = entry.path();
            debug!("discover_site_structure: Examining path {:?}", path);
            if path.is_dir() {
                let path_name = String::from(path.file_name().unwrap().to_str().unwrap());
                if path_name == PATH_PAGES {
                    debug!("discover_site_structure: Found pages directory");
                    structure.pages = true;
                } else if path_name == PATH_POSTS {
                    debug!("discover_site_structure: Found posts directory");
                    structure.posts = true;
                } else if path_name == PATH_TEMPLATES {
                    debug!("discover_site_structure: Found templates directory");
                    structure.templates = true;
                } else if path != config.output_path() && !path_name.starts_with('_') {
                    debug!(
                        "discover_site_structure: Found a custom directory {:?}",
                        path_name
                    );
                    structure.custom.push(path_name);
                }
            }
        }
    }
    if structure.posts {
        discover_posts(config, &mut structure)?;
    }
    Ok(structure)
}

fn discover_posts(config: &SiteConfig, structure: &mut SiteStructure) -> Result<(), Error> {
    let posts_path = make_posts_path(&config.base_path());
    discover_posts_internal(config, structure, &posts_path, 0)?;
    structure.posts_rec.sort_by_key(|p| p.metadata.datetime);
    structure.posts_rec.reverse();
    Ok(())
}

fn discover_posts_internal(
    config: &SiteConfig,
    structure: &mut SiteStructure,
    path: &PathBuf,
    depth: i32,
) -> Result<(), Error> {
    debug!(
        "discover_posts[{}]: Looking for posts in {:?} ...",
        depth, path
    );
    for post in path
        .read_dir()
        .map_err(|e| Error(format!("Unable to read directory: {:?}", e)))?
    {
        if let Ok(post) = post {
            let ft = post
                .file_type()
                .map_err(|e| Error(format!("Unable to read directory: {:?}", e)))?;
            if ft.is_dir() {
                discover_posts_internal(config, structure, &post.path(), depth + 1)?;
            } else if ft.is_file() {
                match post.path().extension() {
                    Some(ext) if ext == MARKDOWN_EXT => {
                        debug!(
                            "discover_posts[{}]: Examining post {:?}",
                            depth,
                            post.path()
                        );
                        let metadata = read_post_metadata(&post.path())?;
                        let dest_url = make_post_url(config, &metadata);
                        let record = PostRecord {
                            source: post.path(),
                            dest_url,
                            metadata: Rc::new(metadata),
                        };
                        structure.posts_rec.push(record);
                    }
                    _ => {}
                }
            }
        }
    }
    Ok(())
}

fn read_post_metadata(post: &PathBuf) -> Result<PostMetadata, Error> {
    debug!("read_post_metadata: Reading post {:?} ...", post);
    let mut file = File::open(post).map_err(|e| Error(format!("Unable to open post: {:?}", e)))?;
    let mut md = String::new();
    file.read_to_string(&mut md)
        .map_err(|e| Error(format!("Unable to read post: {:?}", e)))?;
    debug!("read_post_metadata: ... separating front matter from content ...");
    match md.find(TOML_SEP) {
        Some(p) => {
            let metadata = toml::from_str(&md[..p])
                .map_err(|e| Error(format!("Unable to parse post: {:?}", e)))?;
            Ok(metadata)
        }
        None => Err(Error(format!("Post contained no front matter {:?}", post))),
    }
}

fn make_post_url(config: &SiteConfig, metadata: &PostMetadata) -> String {
    let mut url = String::new();
    url.push_str(
        &metadata
            .datetime
            .format(&config.posts.slug_format)
            .to_string(),
    );
    let slug = match metadata.slug.as_ref() {
        Some(s) => util::make_slug(s),
        None => util::make_slug(&metadata.title),
    };
    match (slug.len(), url.find(SLUG_TOKEN)) {
        (s, Some(p)) if s > 0 => {
            url.replace_range(p..p + SLUG_TOKEN.len(), &slug);
        }
        (_, _) => {}
    }
    if !url.ends_with('/') {
        url.push('/');
    }
    url
}
