extern crate chrono;
extern crate comrak;
extern crate env_logger;
extern crate handlebars;
#[macro_use]
extern crate log;
extern crate serde;
extern crate toml;

mod config;
mod renderer;
mod structure;
mod util;

fn main() {
    env_logger::init();
    let site_config = config::load_config().expect("Unable to load site configuration.");
    let site_struct = structure::discover_site_structure(&site_config)
        .expect("Unable to read directory structure.");

    let mut renderer = renderer::Renderer::new(site_config, site_struct);
    renderer
        .load_templates()
        .expect("Unable to load templates.");
    renderer.walk_pages().expect("Unable to render pages.");
    renderer.walk_custom().expect("Unable to copy assets.");
    renderer.walk_posts().expect("Unable to render blog posts.");
}
