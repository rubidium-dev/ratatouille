use super::{Renderer, TOML_SEP};
use crate::config::SiteConfig;
use crate::util::{self, Error};
use chrono::{DateTime, FixedOffset};
use comrak::{markdown_to_html, ComrakOptions};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::rc::Rc;

static POST_FILE_NAME: &str = "index";

#[derive(Serialize)]
struct PostContext {
    post: Rc<PostMetadata>,
    config: Rc<SiteConfig>,
    body: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PostMetadata {
    pub title: String,
    pub slug: Option<String>,
    pub datetime: DateTime<FixedOffset>,
    pub tags: Vec<String>,
    #[serde(flatten, default)]
    pub custom: HashMap<String, toml::Value>,
}

pub fn make_posts(renderer: &Renderer) -> Result<(), Error> {
    let base_path = renderer.site_config.output_path();
    for post in &renderer.site_structure.posts_rec {
        let dest = base_path
            .join(&post.dest_url[1..])
            .join(POST_FILE_NAME)
            .with_extension(&renderer.site_config.output.ext);
        debug!("make_posts: Rendering {:?} ...", dest);
        util::ensure_path_exists(&dest, true)?;
        let mut file =
            File::open(&post.source).map_err(|e| Error(format!("Unable to open post: {:?}", e)))?;
        let mut md = String::new();
        file.read_to_string(&mut md)
            .map_err(|e| Error(format!("Unable to read source file: {:?}", e)))?;
        debug!("make_posts: ... separating front matter from content ...");
        let prepared_md = match md.find(TOML_SEP) {
            Some(p) => &md[p + TOML_SEP.len()..],
            None => &md[..],
        };
        debug!("make_posts: ... building post context and converting markdown to HTML ...");
        let data = PostContext {
            post: Rc::clone(&post.metadata),
            config: Rc::clone(&renderer.site_config),
            body: markdown_to_html(prepared_md, &ComrakOptions::default()),
        };

        debug!("make_posts: ... writing to destination file ...");
        let mut out_file = File::create(dest)
            .map_err(|e| Error(format!("Unable to open destination file: {:?}", e)))?;

        renderer
            .handlebars
            .render_to_write(
                &renderer.site_config.templates.post_default,
                &data,
                &mut out_file,
            )
            .map_err(|e| Error(format!("Unable to render post template: {:?}", e)))?;
        debug!("make_posts: ... done.");
    }
    Ok(())
}
