use crate::config::SiteConfig;
use crate::util::{self, Error};
use super::{Renderer, TOML_SEP};
use comrak::{markdown_to_html, ComrakOptions};
use serde::Serialize;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::rc::Rc;

#[derive(Serialize)]
struct PageContext {
    page: HashMap<String, toml::Value>,
    config: Rc<SiteConfig>,
    body: String,
}

pub fn render_page(renderer: &Renderer, source: &PathBuf, dest: &PathBuf) -> Result<(), Error> {
    if util::up_to_date(source, dest)? {
        debug!("render_page: Skipping {:?}, output is current.", dest);
        return Ok(());
    }
    debug!("render_page: Rendering {:?} to {:?} ...", source, dest);
    util::ensure_path_exists(dest, true)?;
    let mut file =
        File::open(source).map_err(|e| Error(format!("Unable to open source file: {:?}", e)))?;
    let mut md = String::new();
    file.read_to_string(&mut md)
        .map_err(|e| Error(format!("Unable to read source file: {:?}", e)))?;
    debug!("render_page: ... separating front matter from content ...");
    let (context, prepared_md) = match md.find(TOML_SEP) {
        Some(p) => {
            let toml: HashMap<String, toml::Value> = toml::from_str(&md[..p])
                .map_err(|e| Error(format!("Unable to parse source file: {:?}", e)))?;
            (toml, &md[p + TOML_SEP.len()..])
        }
        None => (HashMap::new(), &md[..]),
    };
    debug!("render_page: ... building page context and converting markdown to HTML ...");
    let data = PageContext {
        page: context,
        config: Rc::clone(&renderer.site_config),
        body: markdown_to_html(prepared_md, &ComrakOptions::default()),
    };

    debug!("render_page: ... writing to destination file ...");
    let mut out_file = File::create(dest)
        .map_err(|e| Error(format!("Unable to open destination file: {:?}", e)))?;

    renderer
        .handlebars
        .render_to_write(
            &renderer.site_config.templates.page_default,
            &data,
            &mut out_file,
        )
        .map_err(|e| Error(format!("Unable to render page template: {:?}", e)))?;
    debug!("render_page: ... done.");
    Ok(())
}
