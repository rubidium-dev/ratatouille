use crate::config::SiteConfig;
use crate::structure::{self, SiteStructure};
use crate::util::*;
use handlebars::Handlebars;
use std::fs;
use std::path::PathBuf;
use std::rc::Rc;

mod helpers;
mod pages;
pub mod posts;

static HBS_EXT: &str = ".hbs";
pub static MARKDOWN_EXT: &str = "md";
pub static TOML_SEP: &str = "\n----\n"; //TODO: handle CRLF as well

pub struct Renderer {
    site_config: Rc<SiteConfig>,
    site_structure: SiteStructure,
    handlebars: Handlebars,
}

impl Renderer {
    pub fn new(site_config: SiteConfig, site_structure: SiteStructure) -> Self {
        let mut handlebars = Handlebars::new();
        handlebars.set_strict_mode(true);
        helpers::register_all(&mut handlebars);

        Renderer {
            site_config: Rc::new(site_config),
            site_structure,
            handlebars,
        }
    }

    pub fn load_templates(&mut self) -> Result<(), Error> {
        if self.site_structure.templates {
            let base_path = self.site_config.base_path();
            let template_path = structure::make_templates_path(&base_path);
            debug!(
                "load_templates: Looking for templates in {:?} ...",
                template_path
            );
            self.handlebars
                .register_templates_directory(HBS_EXT, template_path)
                .map_err(|e| Error(format!("Unable to register templates: {:?}", e)))?;
            #[cfg(debug_assertions)]
            {
                for name in self.handlebars.get_templates().keys() {
                    debug!("load_templates: Loaded '{}'.", name);
                }
            }
            debug!("load_templates: ... done.");
        }
        Ok(())
    }

    pub fn walk_custom(&self) -> Result<(), Error> {
        let base_path = self.site_config.base_path();
        for folder in &self.site_structure.custom {
            let custom_path = structure::make_custom_path(&base_path, &folder);
            walk_custom_internal(&self, &custom_path, 0)?;
        }
        Ok(())
    }

    pub fn walk_pages(&self) -> Result<(), Error> {
        if self.site_structure.pages {
            let base_path = self.site_config.base_path();
            let pages_path = structure::make_pages_path(&base_path);
            walk_pages_internal(&self, &pages_path, 0)?;
        }
        Ok(())
    }

    pub fn walk_posts(&self) -> Result<(), Error> {
        if self.site_structure.posts {
            posts::make_posts(&self)?;
        }
        Ok(())
    }
}

fn walk_custom_internal(
    renderer: &Renderer,
    custom_path: &PathBuf,
    depth: i32,
) -> Result<(), Error> {
    debug!(
        "walk_custom[{}]: Looking for assets in {:?} ...",
        depth, custom_path
    );
    for f in custom_path
        .read_dir()
        .map_err(|e| Error(format!("Unable to read asset directory: {:?}", e)))?
    {
        let f = f.map_err(|e| Error(format!("Unable to read asset directory: {:?}", e)))?;
        let ft = f
            .file_type()
            .map_err(|e| Error(format!("Unable to read asset directory: {:?}", e)))?;
        if ft.is_dir() {
            walk_custom_internal(&renderer, &f.path(), depth + 1)?;
        } else if ft.is_file() {
            let dest = structure::make_output_path(
                &renderer.site_config.base_path(),
                &f.path(),
                &renderer.site_config.output_path(),
            );
            if up_to_date(&f.path(), &dest)? {
                debug!(
                    "walk_custom[{}]: Skipping {:?}, output is current.",
                    depth, dest
                );
            } else {
                debug!("walk_custom[{}]: Copying asset {:?}", depth, f.path());
                ensure_path_exists(&dest, true)?;
                fs::copy(&f.path(), &dest)
                    .map_err(|e| Error(format!("Unable to copy asset: {:?}", e)))?;
            }
        }
    }
    Ok(())
}

fn walk_pages_internal(renderer: &Renderer, pages_path: &PathBuf, depth: i32) -> Result<(), Error> {
    debug!(
        "walk_pages[{}]: Looking for pages in {:?} ...",
        depth, pages_path
    );
    for f in pages_path
        .read_dir()
        .map_err(|e| Error(format!("Unable to read pages directory: {:?}", e)))?
    {
        let f = f.map_err(|e| Error(format!("Unable to read pages directory: {:?}", e)))?;
        let ft = f
            .file_type()
            .map_err(|e| Error(format!("Unable to read pages directory: {:?}", e)))?;
        if ft.is_dir() {
            walk_pages_internal(&renderer, &f.path(), depth + 1)?;
        } else if ft.is_file() {
            match f.path().extension() {
                Some(ext) if ext == MARKDOWN_EXT => {
                    let dest = structure::make_output_path_with_ext(
                        &structure::make_pages_path(&renderer.site_config.base_path()),
                        &f.path(),
                        &renderer.site_config.output_path(),
                        &renderer.site_config.output.ext,
                    );
                    pages::render_page(&renderer, &f.path(), &dest)?;
                }
                _ => {}
            }
        }
    }
    Ok(())
}
