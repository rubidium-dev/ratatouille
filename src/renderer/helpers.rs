use chrono::{DateTime, FixedOffset};
use handlebars::*;

pub fn register_all(handlebars: &mut Handlebars) {
    handlebars.register_helper("formatdt", Box::new(FormatDatetimeHelper));
}

#[derive(Clone, Copy)]
struct FormatDatetimeHelper;

impl HelperDef for FormatDatetimeHelper {
    fn call<'reg: 'rc, 'rc>(
        &self,
        h: &Helper,
        _: &Handlebars,
        c: &Context,
        rc: &mut RenderContext,
        out: &mut Output,
    ) -> HelperResult {
        let dt = h
            .param(0)
            .map(|p| p.value().as_str().unwrap())
            .ok_or(RenderError::new("No date/time provided."))?;
        let fmt = match h.param(1) {
            Some(p) => String::from(p.value().as_str().unwrap()),
            None => get_default_datetime_format(rc, c)?,
        };
        debug!("FormatDateTimeHelp::call: {} {}", dt, fmt);
        let dt = dt
            .parse::<DateTime<FixedOffset>>()
            .map_err(|e| RenderError::new(format!("Unable to parse date/time: {:?}", e)))?;
        let formatted = dt.format(&fmt).to_string();

        out.write(&formatted)?;
        Ok(())
    }
}

fn get_default_datetime_format(
    rc: &RenderContext,
    context: &Context,
) -> Result<String, RenderError> {
    let result = rc.evaluate(context, "config.templates.datetime_format_default")?;
    match result {
        Some(v) => Ok(String::from(v.as_str().unwrap())),
        None => Err(RenderError::new(
            "Unable to retrieve default date/time format.",
        )),
    }
}
