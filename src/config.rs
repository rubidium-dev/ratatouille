use crate::util::Error;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

static CONFIG_NAME: &str = "Site.toml";
static HTML_EXT: &str = "html";
static TEMPL_PAGE_DEF: &str = "page";
static TEMPL_POST_DEF: &str = "post";
static OUTPUT_PATH: &str = "_site";
static POSTS_SLUG_FORMAT: &str = "/%Y/%m/%d/{{slug}}";
static DEFAULT_DATE_FMT: &str = "%B %e, %Y at %l:%M %P";

#[derive(Debug, Deserialize, Serialize)]
pub struct SiteConfig {
    #[serde(skip, default)]
    base_path: Option<Box<PathBuf>>,
    pub site: SiteConfigSite,
    #[serde(default = "templates_default")]
    pub templates: SiteConfigTemplates,
    #[serde(default = "posts_default")]
    pub posts: SiteConfigPosts,
    #[serde(default = "output_default")]
    pub output: SiteConfigOutput,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SiteConfigSite {
    pub title: String,
    #[serde(default)]
    pub desc: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SiteConfigOutput {
    #[serde(default = "output_path_default")]
    pub path: String,
    #[serde(default = "output_ext_default")]
    pub ext: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SiteConfigPosts {
    #[serde(default = "posts_slug_format_default")]
    pub slug_format: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct SiteConfigTemplates {
    #[serde(default = "templates_page_default")]
    pub page_default: String,
    #[serde(default = "templates_post_default")]
    pub post_default: String,
    #[serde(default = "templates_datetime_format_default")]
    pub datetime_format_default: String,
}

impl SiteConfig {
    pub fn base_path(&self) -> PathBuf {
        match self.base_path.as_ref() {
            Some(p) => *p.clone(),
            _ => panic!("base_path was never set!"),
        }
    }

    pub fn output_path(&self) -> PathBuf {
        let output = Path::new(&self.output.path);
        if output.is_absolute() {
            output.to_owned()
        } else {
            self.base_path().join(output).to_owned()
        }
    }

    pub fn from_file(filename: &str) -> Result<SiteConfig, Error> {
        let mut file = File::open(filename)
            .map_err(|e| Error(format!("Unable to open site config {}: {:?}", filename, e)))?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .map_err(|e| Error(format!("Unable to read site config {}: {:?}", filename, e)))?;
        let config: SiteConfig = toml::from_str(&contents)
            .map_err(|e| Error(format!("Unable to parse site config {}: {:?}", filename, e)))?;
        Ok(config)
    }
}

pub fn load_config() -> Result<SiteConfig, Error> {
    let args: Vec<String> = env::args().skip(1).collect();
    let mut path = Path::new("./");
    for a in &args {
        if !a.starts_with('-') {
            path = Path::new(a);
            break;
        }
    }
    let path = path
        .join(CONFIG_NAME)
        .canonicalize()
        .map_err(|e| Error(format!("Bad config path {:?}", e)))?;
    debug!("load_config: Trying path: {:?}", path);
    let mut config = SiteConfig::from_file(&path.to_str().unwrap())?;
    config.base_path = Some(Box::new(path.parent().unwrap().to_owned()));
    debug!("load_config: Site base path: {:?}", config.base_path());
    Ok(config)
}

fn output_default() -> SiteConfigOutput {
    SiteConfigOutput {
        path: output_path_default(),
        ext: output_ext_default(),
    }
}

fn output_path_default() -> String {
    String::from(OUTPUT_PATH)
}

fn output_ext_default() -> String {
    String::from(HTML_EXT)
}

fn templates_default() -> SiteConfigTemplates {
    SiteConfigTemplates {
        page_default: templates_page_default(),
        post_default: templates_post_default(),
        datetime_format_default: templates_datetime_format_default(),
    }
}

fn templates_page_default() -> String {
    String::from(TEMPL_PAGE_DEF)
}

fn templates_post_default() -> String {
    String::from(TEMPL_POST_DEF)
}

fn templates_datetime_format_default() -> String {
    String::from(DEFAULT_DATE_FMT)
}

fn posts_default() -> SiteConfigPosts {
    SiteConfigPosts {
        slug_format: posts_slug_format_default(),
    }
}

fn posts_slug_format_default() -> String {
    String::from(POSTS_SLUG_FORMAT)
}
