use std::path::PathBuf;

#[derive(Debug)]
pub struct Error(pub String);

pub fn make_slug(text: &str) -> String {
    let mut slug = String::new();
    let mut last_was_hyphen = true;
    for c in text.chars() {
        if c.is_alphanumeric() {
            for d in c.to_lowercase() {
                slug.push(d);
            }
            last_was_hyphen = false;
        } else if !last_was_hyphen {
            slug.push('-');
            last_was_hyphen = true;
        }
    }
    if slug.ends_with('-') {
        slug.pop();
    }
    slug
}

pub fn ensure_path_exists(dest: &PathBuf, includes_file_name: bool) -> Result<(), Error> {
    let skip = match includes_file_name {
        true => 1,
        false => 0,
    };
    let dest: Vec<_> = dest.ancestors().skip(skip).take(1).collect();
    if dest.len() != 1 {
        return Err(Error(format!("Cannot create path: no ancestors")));
    }
    if !dest[0].exists() {
        debug!("ensure_path_exists: Creating {:?}", dest[0]);
        std::fs::create_dir_all(&dest[0])
            .map_err(|e| Error(format!("Cannot create path: {:?}", e)))?;
    }
    Ok(())
}

pub fn up_to_date(source: &PathBuf, dest: &PathBuf) -> Result<bool, Error> {
    if dest.exists() {
        let source_modified = source
            .metadata()
            .map_err(|e| Error(format!("Unable to determine source modified date: {:?}", e)))?
            .modified();
        let dest_modified = dest
            .metadata()
            .map_err(|e| {
                Error(format!(
                    "Unable to determine destination modified date: {:?}",
                    e
                ))
            })?
            .modified();
        match (source_modified, dest_modified) {
            (Ok(s), Ok(d)) if s < d => Ok(true),
            (Err(s), _) => Err(Error(format!("Unable to determine source modified date: {:?}", s))),
            (_, Err(d)) => Err(Error(format!(
                "Unable to determine destination modified date: {:?}",
                d
            ))),
            (_, _) => Ok(false),
        }
    } else {
        Ok(false)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn make_slug_ok() {
        assert_eq!("hello-world", make_slug("Hello, world!"));
        assert_eq!("bonjour-le-monde-en-français-ça-va", make_slug("Bonjour, le monde en français! Ça va."));
        assert_eq!("", make_slug("@#$*(#@$#@!*"));
    }
}