# Ratatouille

Ratatouille is a static site builder written in Rust. It uses a combination of TOML metadata and [Handlebars
templates](https://handlebarsjs.com/) to output a set of static HTML files that can be uploaded to a web host.

[![pipeline status](https://gitlab.com/rubidium-dev/ratatouille/badges/master/pipeline.svg)](https://gitlab.com/rubidium-dev/ratatouille/commits/master)